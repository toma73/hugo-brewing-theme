+++
title = "Stout"
featuredImage = "/images/beer-1.png"
description = "Voimakkaan värinen, musta ja paahtunut stout on tyypillisimmillään irlantilaista."
type = ["featured"]
tags = [
    "beer",
    "brewing",
]
date = "2021-04-09"
categories = [
    "Drinks",
]
series = ["Brewing 101"]
[ author ]
  name = "Tom Liimatta"
+++

### STOUT | JÖJJE'S voimahuurteinen!
Voimakkaan värinen, musta ja paahtunut stout on tyypillisimmillään irlantilaista. Kuivahkolla ja vahvalla stoutilla on pitkät perinteet Brittein saarilla.

\- Brewing Bros