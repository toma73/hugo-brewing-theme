---
title: LAGER
date: 2021-02-27T17:38:31.875Z
type: "featured"
featuredImage: /images/beer-2.png
description: Alkuperä oluelle yleensä ottaen varsin nuori. Maailmanvalloitus alkoi vasta 1800-luvulla.
tags:
  - beer
categories:
  - drinks
---
### LAGER - LANCASTER | JÖJJE'S Mahtava Ohris!

Lager on pohjahiivaoluiden yleisnimi ja yleisimmin nautittu olut maailmalla. Sen alkuperä on oluelle yleensä ottaen varsin nuori: se aloitti maailmanvalloituksensa vasta 1800-luvulla. Lager (saks. varasto) viittaa sellaisiin oluttyyppeihin, joiden varastokäyminen tapahtuu viileässä ja on siksi melko pitkä.

\- Brewing Bros