---
title: LAMBIC
date: 2021-02-27T17:38:31.875Z
type: "featured"
featuredImage: /images/beer-3.png
description: Asiallisen mieleenpainuva gut gut hopseista uutettua mallasuutettu
tags:
  - beer
categories:
  - drinks
---
### LAMBIC - INHAHTAVAN GUT GUT JUOMA!
Spontaanilla käymisellä, ilman puhdasviljelyhiivaa valmistettu vehnäolut Sennen laaksosta, Belgiasta. Raaka-aineena on esim. 60 % ohramallasta ja 40 % raakavehnää. Mausteet antavat oman värinsä ja makunsa olueen. Gueuze on maustamaton ja happaman makuinen. Miedompaa ja sokerilla maustettua “kansanversiota” kutsutaan Faroksi. Framboise on maustettu vadelmalla, kriek kirsikalla ja cassis mustaherukalla.

\- Brewing Bros