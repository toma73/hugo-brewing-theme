+++
title = "La Mancha Kotiviini"
cover = "/images/featured-lamanchawine.png"
CoverCaption = "The Catastrophe"
linktitle = "La Mancha punaviini"
type = ["posts","post"]
tags = [
    "brewing",
]
date = "2021-04-07T21:38:31.875Z"
categories = [
    "Drinks",
]
series = ["Brewing 101"]
[ author ]
  name = "Tom Liimatta"
+++


## Introduction

1. Pese Käymisastia vedellä ja desinfiointiaineella
2. Kaada mehu käymisastiaan (jos haluat vahvempaa, lisää 1,5kg sokeria. Lisää vettä 23 l (täydellisempi viini 20 l) Ei saa ylittää 27 asteen lämpöä. Sekoita/ravista liemi kunnolla. Hiivamittarilla voit tarkistaa sokeripitoisuuden. Lisää pussi 7E (Jäst&Nöring). Laita käymisastia sopivaan tasaiseen lämpöpaikkaan (elä ylitä 27 astetta)
3. Ensimmäinen päivä kansi ei saa olla suljettuna, vesilukkoreikään voi laittaa paperia. 2 päivänä lisää vesilukko (jässrör), joka täytettynä puoliksi vedellä. (3 päivänä tarkista käymisen taso). 2 päivän kuluttua pitäisi käynisen olla käymistä. Katso pintaa, onko vaahtoa tms.
4. 3-5 päivän kuluttua, käymisen pitäisi olla loppu, ellei tee vahvan ohjeen mukaan, silloin pari päivää lisää (5-7). Käytä hiivamittaria, joka näyttää nollaa tai alle. Hiivamittarin ylempi osa on tumma/musta, jossa miinus-lukuja. Käymisen loputtua, pitäiso olla 0- tai alle eli miinus. 
5. Kun liemi valmistunut, kaada VAROVASTI toiseen puhtaaseen astiaan. Kun siirretty, lisää käymisen-estoaine (JÄSSTOP H) puhtaaseen astiaan liemen mukaan. Sekoita liemi.
6. Laita pussi 7J (Klarning eli kirkaste), sekoita! Sitten pussi 7K (Klarning- eli kirkaste). Sekoita todella hyvin! Voit maustaa viinin esim.  Tammipurulla?! Koko pönikkää pitää ravistaa ensimmäisen päivän aikana monta kertaa hiilihapon poistamiseksi! 
7. Anna viinin kirkastua korkealla kylmällä ja rauhallisella paikalla 2pv ilman mitään siirtelyä!
8. Kun viini kirkasta, siirrä toiseen astiaan ja vältä sakan/tammihipsujen siirtymistä. Jos hiilihappoja, ravista ne pois. Maista viiniä, lisää tarvittaessa jälkimakeutta. Vahvoille viineille löytyy omia aromeja.
9. Pullota/Bagita neste. Voit vielä antaa muhia pulloissa 3-5 viikkoa
