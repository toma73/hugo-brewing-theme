+++
title = "About"
date = "2021-04-09"
type = "page"
[ author ]
  name = "Brewing bros"
+++

## Panopäivänä välttämättömät välineet

Olutuute ja tarpeelliset lisätilpehöörit. Näiden yhdistelemisestä enemmän erillisessä reseptiikka-artikkelissa. Tärkein sääntö tuossa kuitenkin käyttää maltillisesti tai ei ollenkaan sokeria ja jos käyttää niin panimo- tai kandisokeria riippuen oluen tyylistä, muscovadoa tai hunajaa voi myös koittaa tietyissä oluissa. Missään nimessä vaan ei mitään kilon mälliä mitä ohjeissa joskus sanotaan, ½kg:kin on yläkanttiin. Tuo tietysti pitää korvata jollain, että oluesta ei tule vetistä. DME (kuiva mallasuute) on aika helppo ratkaisu. Nestemäinen mallasuutepurkki on kilohinnaltaan yleensä halvempi.

### Vesilukollinen käymisastia.
### Vedenkeitin tai kattila vedenlämmitykseen.
### Tölkinavaaja, jos sattuu olemaan purkkeja joiden avaamiseen sellaisia tarvitsee. Gozdawan purkkien kanssa tarpeeton.
### 2 Lusikkaa, mieluiten tee- ja ruokalusikka
### Noin 2dl lämmönkestävä lasi tai kahvi/teekuppi
### Lasin/kupin peittämiseen riittävä pala alumiinifoliota
### Iso lusikka, mela tai muu kapusta 
tarvittava koko riippuu vierteen määrästä, jolla vierrettä voi sekoittaa. Materiaalina mieluiten rosteri tai muovi, jotta desinfiointi on helpompaa. Puumelat sitten mäskäyshommiin.
### Desinfiointiainetta
mieluiten sellaista jota ei tarvitse huuhdella kuten Star San tai Chemipro Oxi. Huuhdeltavakin käy, mutta aiheuttaa lisää työtä. Hätätilanteessa kloori on myös erittäin toimiva ratkaisu, maku on tosin kammottava jos sitä ei huuhtele kunnolla.
### Suihkupullo ja rätti desinfiointiaineen levittämiseen.
### Pari patalappua tai paksuja hanskoja
### Sakset
### Muuta tarpeellista muttei välttämätöntä
Ominaispainomittari
Mittakannu, mitä ISOMPI, sitä PAREMPI!
Kannellinen ja hanallinen n. 10l astia desinfiointiaineen säilöntään
Digitaalinen lämpömittari

