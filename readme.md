# Hugo Brewing Theme

Hugo theme with everything you need to get started your brewing site up.

## Getting started

Inside your Hugo site root folder, copy the theme to your `themes` folder.

```bash
git clone https://gitlab.com/toma73/hugo-brewing-theme.git themes/hugo-brewing
```

If you'd like some example content and an example config file to get started, you can copy the `exampleSite` directory into your root Hugo directory.

```bash
cp -r themes/hugo-brewing/exampleSite/* ./
```

To learn more about building themes in Hugo, refer to Hugo's [templating documentation](https://gohugo.io/templates/).
